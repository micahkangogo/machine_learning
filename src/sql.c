


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sqlite3.h>

#include "sql.h"


//rPtr new_route;


/*
int callback(void *NotUsed, int argc, char **argv, char **azColName) {

	char * arg = (char*) NotUsed;

	if(strcmp(arg,"node_q")==0){

		rPtr new_r = (rPtr)malloc(sizeof(route));
		new_r->Q = (qPtr)malloc(sizeof(q));
		new_r->Q->N = (nPtr)malloc(sizeof(nod));

		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"r_id")==0){
				new_r->Q->N->r_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"b_id")==0){
				new_r->Q->N->b_id = atoi(argv[i]);
				new_r->Q->q_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"n_id")==0){
				new_r->Q->N->n_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"u_id")==0){
				new_r->Q->N->u_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"p_id")==0){
				new_r->Q->N->p_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"s_id")==0){
				new_r->Q->N->s_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"tx_id")==0){
				bzero(new_r->Q->N->tx_id,10);
				strcpy(new_r->Q->N->tx_id,argv[i]);
			}
			if(strcmp(azColName[i],"tl_id")==0){
				new_r->Q->N->tl_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"st_num")==0){
				new_r->Q->N->store_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"till_num")==0){
				new_r->Q->N->till_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"amount")==0){
				new_r->Q->N->amount = atof(argv[i]);
			}
			if(strcmp(azColName[i],"n_timestamp")==0){
				bzero(new_r->Q->N->n_timestamp,20);
				strcpy(new_r->Q->N->n_timestamp,argv[i]);
			}
			if(strcmp(azColName[i],"n_status")==0){
				new_r->Q->N->status = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"d_id")==0){
				bzero(new_r->Q->N->d_id,20);
				strcpy(new_r->Q->N->d_id, argv[i] ? argv[i] : "NULL");
			}
		}

		n_enqueue(new_r->r_id,&(new_r->Q->N),new_r->Q->q_id,"init");
	}


	if(strcmp(arg,"node_n")==0){

		rPtr new_r = (rPtr)malloc(sizeof(route));
		new_r->Q = (qPtr)malloc(sizeof(q));
		new_r->Q->N = (nPtr)malloc(sizeof(nod));

		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"r_id")==0){
				new_r->Q->N->r_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"b_id")==0){
				new_r->Q->N->b_id = atoi(argv[i]);
				new_r->Q->q_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"n_id")==0){
				new_r->Q->N->n_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"u_id")==0){
				new_r->Q->N->u_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"p_id")==0){
				new_r->Q->N->p_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"s_id")==0){
				new_r->Q->N->s_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"tx_id")==0){
				bzero(new_r->Q->N->tx_id,10);
				strcpy(new_r->Q->N->tx_id,argv[i]);
			}
			if(strcmp(azColName[i],"tl_id")==0){
				new_r->Q->N->tl_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"st_num")==0){
				new_r->Q->N->store_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"till_num")==0){
				new_r->Q->N->till_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"amount")==0){
				new_r->Q->N->amount = atof(argv[i]);
			}
			if(strcmp(azColName[i],"n_timestamp")==0){
				bzero(new_r->Q->N->n_timestamp,20);
				strcpy(new_r->Q->N->n_timestamp,argv[i]);
			}
			if(strcmp(azColName[i],"n_status")==0){
				new_r->Q->N->status = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"d_id")==0){
				bzero(new_r->Q->N->d_id,20);
				strcpy(new_r->Q->N->d_id, argv[i] ? argv[i] : "NULL");
			}
		}

		HASH_ADD(hh, N , tx_id, strlen(new_r->Q->N->tx_id), new_r->Q->N);
	}

	else if(strcmp(arg,"users")==0){

		uPtr new_user = (uPtr)malloc(sizeof(user));
		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"u_fname")==0){
				bzero(new_user->f_name,20);
				strcpy(new_user->f_name,argv[i]);
			}
			if(strcmp(azColName[i],"u_lname")==0){
				bzero(new_user->l_name,20);
				strcpy(new_user->l_name,argv[i]);
			}
			if(strcmp(azColName[i],"u_pass")==0){
				new_user->u_pass = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"u_name")==0){
				bzero(new_user->u_name,20);
				strcpy(new_user->u_name,argv[i]);
			}
			if(strcmp(azColName[i],"u_id")==0){
				new_user->u_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"u_role")==0){
				new_user->u_role = atoi(argv[i]);
				if(new_user->u_role == 1000)
					RET = 1000;
			}
			if(strcmp(azColName[i],"u_timestamp")==0){
				strcpy(new_user->u_timestamp,argv[i]);
			}
			if(strcmp(azColName[i],"r_id")==0){
				new_user->r_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"p_id")==0){
				new_user->p_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"s_id")==0){
				new_user->s_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"store_num")==0){
				new_user->store_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"till_num")==0){
				new_user->till_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"wallet_id")==0){
				bzero(new_user->wallet_id,128);
				strcpy(new_user->wallet_id,argv[i]);
			}
			if(strcmp(azColName[i],"app_id")==0){
				new_user->app_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"d_id")==0){
				bzero(new_user->d_id,10);
				strcpy(new_user->d_id,argv[i]);
			}
			if(strcmp(azColName[i],"wallet_val")==0){
				new_user->wallet_val = atof(argv[i]);
			}
		}

		HASH_ADD(hh, U, u_id, sizeof(unsigned int),new_user);
	}

	else if(strcmp(arg,"block")==0){

		new_route->Q = (qPtr)malloc(sizeof(q));
		new_route->Q->front = new_route->Q->rear = NULL;
		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"b_id")==0){
				new_route->Q->q_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"b_name")==0){
				strcpy(new_route->Q->q_name,argv[i]);
			}
			if(strcmp(azColName[i],"n_num")==0){
				new_route->Q->n_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"b_timestamp")==0){
				bzero(new_route->Q->q_timestamp,10);
				strcpy(new_route->Q->q_timestamp,argv[i]);
			}
			if(strcmp(azColName[i],"r_id")==0){
				new_route->r_id = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"checkout_count")==0){
				new_route->Q->c_num = atoi(argv[i]);
			}
		}

		q_enqueue(new_route ,new_route->Q,"init");

		//new_route->Q->n_num ++;

		R->q_num ++;
		q_n_pairs(new_route->r_id,new_route->Q->q_id, R->q_num, new_route->Q->n_num,new_route->Q->c_num);

		puts("VVVVVVVVVVVVVVV");

	}

	else if(strcmp(arg,"route")==0){

		rPtr new_route = (rPtr)malloc(sizeof(route));

		RET++;

		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"r_id")==0){
				new_route->r_id = atoi(argv[i]);
			}

			if(strcmp(azColName[i],"r_name")==0){
				strcpy(new_route->r_name , (argv[i]));
			}

			if(strcmp(azColName[i],"b_num")==0){
				new_route->q_num = atoi(argv[i]);
			}

			if(strcmp(azColName[i],"r_port")==0){
				new_route->r_port = atoi(argv[i]);
			}

			if(strcmp(azColName[i],"r_timestamp")==0){
				strcpy(new_route->r_timestamp , (argv[i]));
			}
		}

		HASH_ADD(hh, R, r_id, sizeof(unsigned int), new_route);

	}

	else if(strcmp(arg,"users")==0){

		rPtr new_route = (rPtr)malloc(sizeof(route));

		RET++;

		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"r_id")==0){
				new_route->r_id = atoi(argv[i]);
			}
		}

		HASH_ADD(hh, R, r_id, sizeof(unsigned int), new_route);

	}


	else if(strcmp(arg,"device")==0){

		dPtr new_device = (dPtr)malloc(sizeof(device));

		RET++;
		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"d_id")==0){

				strcpy(new_device->d_id , (argv[i]));
			}

			if(strcmp(azColName[i],"d_name")==0){
				strcpy(new_device->d_name , (argv[i]));

			}

			if(strcmp(azColName[i],"d_loc")==0){
				strcpy(new_device->d_loc , (argv[i]));
			}

			if(strcmp(azColName[i],"d_status")==0){
				new_device->d_status = atoi(argv[i]);
			}

			if(strcmp(azColName[i],"d_active")==0){
				new_device->d_active = atoi(argv[i]);
			}

			if(strcmp(azColName[i],"d_timestamp")==0){
				strcpy(new_device->d_timestamp , (argv[i]));
			}

			if(strcmp(azColName[i],"d_longitude")==0){
				strcpy(new_device->d_longitude , (argv[i]));
			}

			if(strcmp(azColName[i],"d_latitude")==0){
				strcpy(new_device->d_latitude , (argv[i]));
			}

			if(strcmp(azColName[i],"d_drive")==0){
				new_device->d_drive = atoi(argv[i]);
			}
		}

		HASH_ADD(hh, D , d_id, sizeof(new_device->d_id), new_device);
	}

	else if(strcmp(arg,"block_update")==0){

		for (int i = 0; i < argc; i++) {

			if(strcmp(azColName[i],"b_id")==0){
			}
			if(strcmp(azColName[i],"b_name")==0){
			}
			if(strcmp(azColName[i],"n_num")==0){
				n_num = atoi(argv[i]);
			}
			if(strcmp(azColName[i],"b_timestamp")==0){

			}
			if(strcmp(azColName[i],"r_id")==0){
			}
			if(strcmp(azColName[i],"checkout_count")==0){

				checkout_count = atoi(argv[i]);
			}
		}
	}

	else{

		 code
	}

	return 0;
}*/

int sqlite_f(char *sql, char * cmd, char *arg) {

	sqlite3 *db;
	char *err_msg = 0;

	int rc = sqlite3_open("ml.db", &db);

	if (rc != SQLITE_OK) {

		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);

		return -1;
	}

	if(strcmp(cmd,"select")==0){
		//rc = sqlite3_exec(db, sql,callback,(char*)arg, &err_msg);
	}
	else
	{
		rc = sqlite3_exec(db, sql,0, 0, &err_msg);
	}

	if (rc != SQLITE_OK ) {

		fprintf(stderr, "SQL error: %s\n", err_msg);
		puts("Error storing data");
		sqlite3_free(err_msg);
		sqlite3_close(db);

		return -1;
	}
	sqlite3_close(db);

	puts("LLL");

	return 0;
}


/*POSTGRES - ftms db
 * +-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| r_id        | int(11)     | YES  |     | NULL    |       |
| b_id        | int(11)     | YES  |     | NULL    |       |
| n_id        | int(11)     | YES  |     | NULL    |       |
| u_id        | int(11)     | YES  |     | NULL    |       |
| p_id        | int(11)     | YES  |     | NULL    |       |
| s_id        | int(11)     | YES  |     | NULL    |       |
| tx_id       | varchar(20) | NO   | PRI | NULL    |       |
| tl_id       | int(11)     | YES  |     | NULL    |       |
| store_num   | mediumtext  | YES  |     | NULL    |       |
| till_num    | mediumtext  | YES  |     | NULL    |       |
| amount      | double      | YES  |     | NULL    |       |
| n_timestamp | varchar(20) | YES  |     | NULL    |       |
| n_status    | int(11)     | YES  |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+*/

/*-- Describe NODE
 * SQLITE ftms db
CREATE TABLE node (
    "r_id" INTEGER NOT NULL,
    "b_id" INTEGER NOT NULL,
    "n_id" INTEGER NOT NULL,
    "u_id" INTEGER NOT NULL,
    "p_id" NULL,
    "s_id" INTEGER,
    "tx_id" TEXT NOT NULL,
    "tl_id" INTEGER,
    "st_num" INTEGER,
    "till_num" INTEGER,
    "amount" REAL,
    "n_timestamp" TEXT NOT NULL,
    "n_status" INTEGER
)
 */
