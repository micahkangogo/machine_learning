//============================================================================
// Name        : machine_learning.cpp
// Author      : Micah Kangogo
// Version     :
// Copyright   : Source
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "gd.h"
#include "ml.h"

int main() {

	GradientDescent g;

	float m = 0;
	float b = 0;

	UT_icd dataSet_icd = {sizeof(data_set), NULL, NULL, NULL};
	utarray_new(pairs,&dataSet_icd);

	UT_icd pd_icd = {sizeof(pd_coeff), NULL, NULL, NULL};
	utarray_new(pd_coeff,&pd_icd);

	g.generateData_("/home/micah/DevOps/C_C++/workspace/ml/src/data.csv");

	g.batchTrain(&m, &b, 0.0001);

	cout << "Value for m in equation y = mx + b --> " << m << endl;

	cout << "Value for b in equation y = mx + b --> " << b << endl;
	printf("\nSize of Data : %u\n",(unsigned int)utarray_len(pairs));

	utarray_free(pairs);
	utarray_free(pd_coeff);
	return 1;
}
