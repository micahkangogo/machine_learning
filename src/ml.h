/*
 * ml.h
 *
 *  Created on: Nov 28, 2019
 *      Author: root
 */

#ifndef ML_H_
#define ML_H_

#include "utarray.h"

typedef struct {

	double x;
	double y;

}data_set;

typedef struct {

	double rM;
	double rB;
	double cost;

}pd_array;

inline UT_array * pd_coeff;

inline UT_array *pairs;

#endif /* ML_H_ */
