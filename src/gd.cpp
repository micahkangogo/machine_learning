/*
 * GradientDescent.cc
 *
 *  Created on: Sep 28, 2019
 *      Author: Micah Kangogo
 */

#include "gd.h"
#include "ml.h"

float sum;

int pd_coeff_insert(double rM, double rB, double cost){

	pd_array PD;

	PD.rM = rM;
	PD.rB = rB;
	PD.cost = cost;

	printf("P.rM : %f\n",PD.rM);
	printf("P.rB : %f\n",PD.rB);
	printf("P.cost : %f\n",PD.cost);

	utarray_push_back(pd_coeff, &PD);

	return 0;
}

// reads the csv file and stores it in the vector containing a vector of array with two elements
// first element being the value of y axis for a specific point and second being the x axis for a specific point

void GradientDescent::generateData_(const char* fileName) {

	char line[1024];
	FILE *f = NULL;
	char *x=NULL,*y=NULL;
	data_set data;

	f = fopen(fileName,"r");

	if(f){

		while (fgets(line, 1024, f) != NULL) {

			x = strtok(line,",");
			if(x){
				y = strtok(NULL,",");
			}

			data.x=atof(x);  data.y=atof(y);
			utarray_push_back(pairs, &data);
		}

		fclose(f);

	}else{

		puts("No file found");
		perror("INFO:");
		return;
	}
	return;
}

// goes through one iteration and applies gradient descent
void GradientDescent::gradDesc(double* m, double* b, float learningRate,double * cost) {
	float gradM = 0; // gradient of M to be accumulated
	float gradB = 0; // gradient of B to be accumulated

	// declaration of the x and y coordinates
	float x = 0;
	float y = 0;

	float y_p = 0;

	float m_c = *m;
	float b_c = *b;

	float sum;

	data_set *p;

	// iterating over the dataset and grabbing values of x and y and calculating
	// the partial derivative of the cost function with respect to the m and then
	// with respect with b

	for(p=(data_set*)utarray_front(pairs);p!=NULL;p=(data_set*)utarray_next(pairs,p)) {
		// assigning the x and y values from the multidimentional array
		x = p->x;
		y = p->y;

		y_p = m_c * x + b_c;
		sum += (y_p - p->y);

		gradM += -(x * (y - ((*m * x) + *b)));
		gradB += -(y - ((*m * x) + *b));

	}

	// type casting an int to a float for the size which is n in the formula
	//float n = static_cast<float>(points.size());

	float n = (float)utarray_len(pairs);
	// calculate the cost
	*cost = (1.0/n) * (sum*sum);

	// averaging the gradient
	gradM = gradM * (2.0 / n);
	gradB = gradB * (2.0 / n);
	// learning the new m and b value
	// changes the value of m and b depending on the gradient of cost function with respect to M and B
	*m = *m - (gradM * learningRate);
	*b = *b - (gradB * learningRate);
}

// function that iterates the dataset a certain number of times and does gradient descent
void GradientDescent::batchTrain(float* m, float* b, float learningRate) {

	double rM = 0; // value to change starts at 0
	double rB = 0; // value to change starts at 0
	double cost = 0; // value to change starts at 0

	// Explicitly deciding how many times that one data set will be iterated
	// this is the key part of gradient descent, each iteration changes the value
	// of m and b, this change improves the m and b over each iterations every time.
	// as gradDesc function finds the derivative of the cost function in terms of
	// m and then b. For each points in the dataset the gradient is found and added and then by using
	// the formula new_m = m - (gradient_of_m * learning_rate), we find the new m and b as well similarly.

	// gradient descent needs to happen multiple times, as we are getting better and better at
	// plotting the line to best fit our dataset, hence we do gradient descent 100000 amount of times

	int iteration_of_same_data_set = 500000;

	for (int i = 0; i < iteration_of_same_data_set; ++i){

		gradDesc(&rM, &rB, learningRate,&cost);

		//pd_coeff_insert(rM,rB,cost);

		printf("%f %f %f\n", rM, rB,cost);
	}

	*m = rM;
	*b = rB;
}
